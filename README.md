# TOMLChef

[![Common Changelog](https://common-changelog.org/badge.svg)](https://common-changelog.org)

A framework for building modular and easily configurable Python applications.

## Installation

To be added.

## Usage

To be added.

## Contributing

Please refer to the [contribution guide](docs/CONTRIBUTING.md).

## Authors and acknowledgment

To be added.

## License

[BSD 3-Clause Clear License](LICENSE)
