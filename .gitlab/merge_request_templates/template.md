# Branch description
Provide a concise overview of the issue or rationale behind the proposed changes.

# Checklist
- [ ] This PR targets the `main` branch. <!-- Backports will be evaluated and done by mergers, when necessary. -->
- [ ] I have rebased my feature branch to squash/fixup any noisy commits ("FIXED ...", "try 2", etc.). <!-- We simply ask for a clean git history, don't squash all your commits into one. -->
- [ ] Each commit message is descriptive and written in [present-tense, imperative-style].
- [ ] I have added or updated relevant tests.
- [ ] I have added or updated relevant docs, including the [CHANGELOG] if applicable.
- [ ] My additions to the changelog (if any) follow the [Common Changelog] format.
- [ ] I have read the rendered preview of this description and checked the relevant links.
<!-- Ignore this section, these are just links for the references above -->
[present-tense, imperative-style]: https://git.kernel.org/pub/scm/git/git.git/tree/Documentation/SubmittingPatches?h=v2.36.1#n181
[CHANGELOG]: ../../docs/CHANGELOG.md
[Common Changelog]: https://github.com/vweevers/common-changelog
