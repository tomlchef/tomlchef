# Changelog

## [0.1.0-a1] - UNDER DEVELOPMENT

_First release._

### Added

- Implement a simple CLI using `click`.

### Added

- Implement a simple CLI using `click` ([#3](https://gitlab.com/tomlchef/tomlchef/-/work_items/3)) ([!5](https://gitlab.com/tomlchef/tomlchef/-/merge_requests/5)) ([bc6fa512](https://gitlab.com/tomlchef/tomlchef/-/commit/bc6fa512b9633c79d80057ee35d1b2fae87a064f))

[0.1.0-a1]: https://gitlab.com/hcording/tomlchef/-/releases/0.1.0-a1
