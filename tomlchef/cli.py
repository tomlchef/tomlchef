import click

CONTEXT_SETTINGS = dict(help_option_names=['-h', '--help'])


def _get_name():
    return "cli"  # TODO - retrieve dynamically from DistributionManger


@click.group(name=_get_name(), context_settings=CONTEXT_SETTINGS)
def cli():
    pass
