import unittest

from click.testing import CliRunner

from tomlchef.cli import cli


class TestCli(unittest.TestCase):
    help_message = """Usage: cli [OPTIONS] COMMAND [ARGS]...

Options:
  -h, --help  Show this message and exit.
"""

    @staticmethod
    def _invalid_command_message(command):
        return f"""Usage: cli [OPTIONS] COMMAND [ARGS]...
Try 'cli -h' for help.

Error: No such command '{command}'.
"""

    @staticmethod
    def _invalid_option_message(option) -> str:
        return f"""Usage: cli [OPTIONS] COMMAND [ARGS]...
Try 'cli -h' for help.

Error: No such option: {option}
"""

    def test_no_arg_prints_help(self):
        result = CliRunner().invoke(cli, [])
        self.assertEqual(0, result.exit_code)
        self.assertEqual(self.help_message, result.output)

    def test_short_help(self):
        result = CliRunner().invoke(cli, ["-h"])
        self.assertEqual(0, result.exit_code)
        self.assertEqual(self.help_message, result.output)

    def test_long_help(self):
        result = CliRunner().invoke(cli, ["--help"])
        self.assertEqual(0, result.exit_code)
        self.assertEqual(self.help_message, result.output)

    def test_invalid_arg(self):
        result = CliRunner().invoke(cli, ["--asdf"])
        self.assertEqual(2, result.exit_code)
        self.assertEqual(self._invalid_option_message("--asdf"), result.output)

        result = CliRunner().invoke(cli, ["-z"])
        self.assertEqual(2, result.exit_code)
        self.assertEqual(self._invalid_option_message("-z"), result.output)

        result = CliRunner().invoke(cli, ["asdf"])
        self.assertEqual(2, result.exit_code)
        self.assertEqual(self._invalid_command_message("asdf"), result.output)
